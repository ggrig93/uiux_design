import './bootstrap';
import '../css/app.css';
import "bootstrap/dist/css/bootstrap.min.css"
import '../../public/global/plugins.bundle.css'
import '../css/main.css';
import '../css/theme.css';
import '../css/table.css';
import '../css/multiSelect.css';
import * as bootstrap from 'bootstrap'

import { createApp, h } from 'vue';
import Notifications from '@kyvg/vue3-notification'
import Vuex from 'vuex'
import store from "./store/index.js"
import Vue3EasyDataTable from 'vue3-easy-data-table';
import Paginate from "vuejs-paginate-next";
import SvgIcon from "vue3-icon";
import VueApexCharts from "vue3-apexcharts";
import { Modal } from 'usemodal-vue3';
import 'vue3-easy-data-table/dist/style.css';
import { createInertiaApp } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';
import { i18nVue } from 'laravel-vue-i18n';
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';
let currentLang = 'en'
if(localStorage.getItem('currentLang') === null){
    localStorage.setItem('currentLang', currentLang)
}else{
    currentLang = localStorage.getItem('currentLang')
}

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        return createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(ZiggyVue, Ziggy)
            .use(Notifications)
            .use(store)
            .use(Vuex)
            .use(Paginate)
            .use(VueApexCharts)
            .use(i18nVue, {
                lang: currentLang,
                resolve: lang => {
                    const langs = import.meta.glob('../../lang/*.json', { eager: true });
                    return langs[`../../lang/${lang}.json`].default;
                },
            })
            .component('VueDatePicker', VueDatePicker)
            .component('EasyDataTable', Vue3EasyDataTable)
            .component('paginate', Paginate)
            .component("svg-icon", SvgIcon)
            .mount(el);
    },
    progress: {
        color: '#4B5563',
    },
});
