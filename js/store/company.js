export default {
    namespaced: true,

    state () {
        return {
            company: {},
            companyDashboard: {},
            ownerCompanies: [],
        }
    },

    getters: {
        companyDashboard: state => state.companyDashboard,
        company: state => state.company,
        ownerCompanies: state => state.ownerCompanies,
    },

    mutations: {
        setCompany(state, value) {
            state.company = value
        },
        setCompanyDashboard(state, value) {
            state.companyDashboard = value
        },
        setOwnerCompanies(state, value) {
            state.ownerCompanies = value
        },
    },

    actions: {
        async getCompany({commit}, id) {
            try {
                const {data} = await axios.get(`/get-company/${id}`)
                commit('setCompany', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
        async getCompanyDashboard({commit}, payload = {}) {
            try {
                const {data} = await axios.post(`/get-company-dashboard/${payload.id}`, payload)
                commit('setCompanyDashboard', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
        async getOwnerCompanies({commit}) {
            try {
                const {data} = await axios.get('/get-owner-companies')
                commit('setOwnerCompanies', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
    },
}
