import product from "./product";
import customer from "./customer";
import order from "./order";
import invoice from "./invoice";
import estimate from "./estimate";
import user from "./user";
import company from "./company";
import country from "./country";
import settings from "./settings";
import vat from "./vat";
import material from "./material";
import membership from "./membership";
import activityLogs from "./activityLogs";
import warehouse from "./warehouse";
import dashboard from "./dashboard";
import selector from "./selector";
import search from "./search";
import supplier from "./supplier";
import supply from "./supply";
import { createStore } from 'vuex'

export default createStore({

    state () {
        return {
            roles: [],
            permissions: [],
        }
    },

    getters: {
        roles: state => state.roles,
        permissions: state => state.permissions,
    },


    mutations: {
        setRoles(state, value) {
            state.roles = value
        },
        setPermissions(state, value) {
            state.permissions = value
        },
    },

    actions: {
        async getRoles({commit}) {
            try {
                const {data} = await axios.get('/get-roles')
                commit('setRoles', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getPermissions({commit}) {
            try {
                const {data} = await axios.get('/get-permissions')
                commit('setPermissions', data)
            } catch (e) {
                console.log(e)
            }
        },
    },

    modules: {
        product,
        customer,
        user,
        company,
        order,
        invoice,
        estimate,
        country,
        settings,
        membership,
        activityLogs,
        dashboard,
        vat,
        material,
        warehouse,
        selector,
        search,
        supplier,
        supply,
    }
})
