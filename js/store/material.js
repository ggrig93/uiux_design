export default {
    namespaced: true,

    state () {
        return {
            materials: [],
            materialList: [],
        }
    },

    getters: {
        materials: state => state.materials,
        materialList: state => state.materialList,
    },

    mutations: {
        setMaterials(state, value) {
            state.materials = value
        },
        setMaterialList(state, value) {
            state.materialList = value
        },
    },

    actions: {
        async getMaterials({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-materials?page=${payload.page}`, payload)
                commit('setMaterials', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getMaterialList({commit}, payload = {}) {
            try {
                const {data} = await axios.post('/get-material-list', payload)
                commit('setMaterialList', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
