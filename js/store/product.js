export default {
    namespaced: true,

    state () {
        return {
            products: {},
            productList: [],
            materials: []
        }
    },

    getters: {
        products: state => state.products,
        productList: state => state.productList,
        materials: state => state.materials,
    },

    mutations: {
        setProducts(state, value) {
            state.products = value
        },
        setProductList(state, value) {
            state.productList = value
        },
        setMaterials(state, value) {
            state.materials = value
        },
    },

    actions: {
        async getProducts({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-products?page=${payload.page}`, payload)
                commit('setProducts', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getProductList({commit}) {
            try {
                const {data} = await axios.get('/get-product-list')
                commit('setProductList', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getMaterials({commit}, productId) {
            try {
                const {data} = await axios.get(`/get-product-materials/${productId}`)
                commit('setMaterials', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
