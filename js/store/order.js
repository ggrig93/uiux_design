export default {
    namespaced: true,

    state () {
        return {
            orders: {},
        }
    },

    getters: {
        orders: state => state.orders,
    },

    mutations: {
        setOrders(state, value) {
            state.orders = value
        },
    },

    actions: {
        async getOrders({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-orders?page=${payload.page}`, payload)
                commit('setOrders', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
