export default {
    namespaced: true,

    state () {
        return {
            supplies: {},
        }
    },

    getters: {
        supplies: state => state.supplies,
    },

    mutations: {
        setSupplies(state, value) {
            state.supplies = value
        },
    },

    actions: {
        async getSupplies({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-supplies?page=${payload.page}`, payload)
                commit('setSupplies', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
