export default {
    namespaced: true,

    state () {
        return {
            companySettings: [],

            companyStatuses: {},
            companyStatusUsed: false,

            salesPipelines: [],
            salesPipelineUsed: false,

            invoiceNotifications: [],
        }
    },

    getters: {
        companySettings: state => state.companySettings,

        companyStatuses: state => state.companyStatuses,
        companyStatusUsed: state => state.companyStatusUsed,

        salesPipelines: state => state.salesPipelines,
        salesPipelineUsed: state => state.salesPipelineUsed,

        invoiceNotifications: state => state.invoiceNotifications,
    },

    mutations: {
        setCompanySettings(state, value) {
            state.companySettings = value
        },

        setCompanyStatuses(state, value) {
            state.companyStatuses = value
        },
        setCompanyStatusUsed(state, value) {
            state.companyStatusUsed = value
        },

        setSalesPipelines(state, value) {
            state.salesPipelines = value
        },
        setSalesPipelineUsed(state, value) {
            state.salesPipelineUsed = value
        },

        setInvoiceNotifications(state, value) {
            state.invoiceNotifications = value
        },
    },

    actions: {
        async getCompanySettings({commit}, companyId) {
            try {
                const {data} = await axios.get(`/get-company-settings/${companyId}`)
                commit('setCompanySettings', data)
            } catch (e) {
                console.log(e)
            }
        },

        async getCompanyStatuses({commit, state}, companyId) {
            try {
                if(Object.keys(state.companyStatuses).length === 0){
                    const {data} = await axios.get(`/get-company-statuses/${companyId}`)
                    commit('setCompanyStatuses', data)
                }
            } catch (e) {
                console.log(e)
            }
        },
        async checkCompanyStatusUsed({commit}, id) {
            try {
                const {data} = await axios.get(`/check-company-status-used/${id}`)
                commit('setCompanyStatusUsed', data)
            } catch (e) {
                console.log(e)
            }
        },

        async getSalesPipelines({commit, state}, companyId) {
            try {
                if(Object.keys(state.salesPipelines).length === 0){
                    const {data} = await axios.get(`/get-sales-pipelines/${companyId}`)
                    commit('setSalesPipelines', data)
                }
            } catch (e) {
                console.log(e)
            }
        },
        async checkSalesPipelineUsed({commit}, id) {
            try {
                const {data} = await axios.get(`/check-sales-pipeline-used/${id}`)
                commit('setSalesPipelineUsed', data)
            } catch (e) {
                console.log(e)
            }
        },


        async getInvoiceNotifications({commit}, companyId) {
            try {
                const {data} = await axios.get(`/get-invoice-notifications/${companyId}`)
                commit('setInvoiceNotifications', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
