export default {
    namespaced: true,

    state () {
        return {
            // activityLogs: [],
            totalOrdersByMonths: [],
            totalEstimatesByMonths: [],
        }
    },

    getters: {
        // activityLogs: state => state.activityLogs,
        totalOrdersByMonths: state => state.totalOrdersByMonths,
        totalEstimatesByMonths: state => state.totalEstimatesByMonths,
    },

    mutations: {
        // setActivityLogs(state, value) {
        //     state.activityLogs = value
        // },

        setTotalOrdersByMonths(state, value) {
            state.totalOrdersByMonths = value
        },

        setTotalEstimatesByMonths(state, value) {
            state.totalEstimatesByMonths = value
        },
    },

    actions: {
        // async getActivityLogs({commit}) {
        //     try {
        //         const {data} = await axios.post('/get-activity-logs')
        //         commit('setActivityLogs', data)
        //         return data
        //     } catch (e) {
        //         console.log(e)
        //     }
        // },

        async getTotalOrdersByMonths({commit}) {
            try {
                const {data} = await axios.post('/get-total-orders-by-months')
                commit('setTotalOrdersByMonths', data)
            } catch (e) {
                console.log(e)
            }
        },

        async getTotalEstimatesByMonths({commit}) {
            try {
                const {data} = await axios.post('/get-total-estimates-by-months')
                commit('setTotalEstimatesByMonths', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
