export default {
    namespaced: true,

    state () {
        return {
            searchProducts: {},
            searchCustomers: {},
            searchOrders: {},
            searchEstimates: {},
            searchWarehouses: {},
            globalSearch: null,
        }
    },

    getters: {
        searchProducts: state => state.searchProducts,
        searchCustomers: state => state.searchCustomers,
        searchOrders: state => state.searchOrders,
        searchEstimates: state => state.searchEstimates,
        searchWarehouses: state => state.searchWarehouses,
        globalSearch: state => state.globalSearch,
    },

    mutations: {
        setSearchProducts(state, value) {
            state.searchProducts = value
        },
        setSearchCustomers(state, value) {
            state.searchCustomers = value
        },
        setSearchOrders(state, value) {
            state.searchOrders = value
        },
        setSearchEstimates(state, value) {
            state.searchEstimates = value
        },
        setSearchWarehouses(state, value) {
            state.searchWarehouses = value
        },
        setGlobalSearch(state, value) {
            state.globalSearch = value
        },
    },

    actions: {
        async getSearchProducts({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/search-products?page=${payload.page}`, payload)
                commit('setSearchProducts', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getSearchCustomers({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/search-customers?page=${payload.page}`, payload)
                commit('setSearchCustomers', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getSearchOrders({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/search-orders?page=${payload.page}`, payload)
                commit('setSearchOrders', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getSearchEstimates({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/search-estimates?page=${payload.page}`, payload)
                commit('setSearchEstimates', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getSearchWarehouses({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/search-warehouses?page=${payload.page}`, payload)
                commit('setSearchWarehouses', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getGlobalSearch({commit}, search) {
            try {
                const {data} = await axios.get(`/global-search/${search}`)
                commit('setGlobalSearch', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
