export default {
    namespaced: true,

    state () {
        return {
            typesWithOptions: [],
        }
    },

    getters: {
        typesWithOptions: state => state.typesWithOptions,
    },

    mutations: {
        setTypesWithOptions(state, value) {
            state.typesWithOptions = value
        },
    },

    actions: {
        async getTypesWithOptions({commit}) {
            try {
                const {data} = await axios.get('/get-selector-types-with-options')
                commit('setTypesWithOptions', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
