export default {
    namespaced: true,

    state () {
        return {
            warehouse: [],
        }
    },

    getters: {
        warehouse: state => state.warehouse,
    },

    mutations: {
        setWarehouse(state, value) {
            state.warehouse = value
        },
    },

    actions: {
        async getWarehouse({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-warehouse?page=${payload.page}`, payload)
                commit('setWarehouse', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
    },
}
