export default {
    namespaced: true,

    state () {
        return {
            suppliers: {},
            supplierList: [],
            supplier: {},
        }
    },

    getters: {
        suppliers: state => state.suppliers,
        supplierList: state => state.supplierList,
    },

    mutations: {
        setSuppliers(state, value) {
            state.suppliers = value
        },
        setSupplierList(state, value) {
            state.supplierList = value
        },
    },

    actions: {
        async getSuppliers({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-suppliers?page=${payload.page}`, payload)
                commit('setSuppliers', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getSupplierList({commit}) {
            try {
                const {data} = await axios.get('/get-supplier-list')
                commit('setSupplierList', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
