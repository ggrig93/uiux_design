export default {
    namespaced: true,

    state () {
        return {
            member: {},
            companyMembers: [],
            filteredMembers: [],
        }
    },

    getters: {
        member: state => state.member,
        companyMembers: state => state.companyMembers,
        filteredMembers: state => state.filteredMembers,
    },

    mutations: {
        setMember(state, value) {
            state.member = value
        },
        setCompanyMembers(state, value) {
            state.companyMembers = value
        },
        setFilteredMembers(state, value) {
            state.filteredMembers = value
        },
    },

    actions: {
        async getMember({commit}, id) {
            try {
                const {data} = await axios.get(`/get-member/${id}`)
                commit('setMember', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getCompanyMembers({commit}, id) {
            try {
                const {data} = await axios.get(`/get-company-members/${id}`)
                commit('setCompanyMembers', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
        async getFilteredMembers({commit}, payload) {
            try {
                const {data} = await axios.post('/get-filtered-members', payload)
                commit('setFilteredMembers', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
    },
}
