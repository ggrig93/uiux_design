export default {
    namespaced: true,

    state () {
        return {
            estimates: {},
        }
    },

    getters: {
        estimates: state => state.estimates,
    },

    mutations: {
        setEstimates(state, value) {
            state.estimates = value
        },
    },

    actions: {
        async getEstimates({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-estimates?page=${payload.page}`, payload)
                commit('setEstimates', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
