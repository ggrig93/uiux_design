export default {
    namespaced: true,

    state () {
        return {
            vatList: [],
            vat: {}
        }
    },

    getters: {
        vatList: state => state.vatList,
        vat: state => state.vat,
    },

    mutations: {
        setVatList(state, value) {
            state.vatList = value
        },
        setVat(state, value) {
            state.vat = value
        },
    },

    actions: {
        async getVatList({commit, state}) {
            try {
                if(!state.vatList.length){
                    const {data} = await axios.get('/get-vat-list')
                    commit('setVatList', data)
                }
            } catch (e) {
                console.log(e)
            }
        },
        async getVat({commit}, payload) {
            commit('setVat', {})
            try {
                const {data} = await axios.post('/get-vat', payload)
                commit('setVat', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
