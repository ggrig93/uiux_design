export default {
    namespaced: true,

    state () {
        return {
            customers: {},
            customerList: [],
            customer: {},
            otherLocations: [],
            peoples: [],
        }
    },

    getters: {
        customers: state => state.customers,
        customerList: state => state.customerList,
        customer: state => state.customer,
        otherLocations: state => state.otherLocations,
        peoples: state => state.peoples,
    },

    mutations: {
        setCustomers(state, value) {
            state.customers = value
        },
        setCustomerList(state, value) {
            state.customerList = value
        },
        setCustomer(state, value) {
            state.customer = value
        },
        setOtherLocations(state, value) {
            state.otherLocations = value
        },
        setPeoples(state, value) {
            state.peoples = value
        },
    },

    actions: {
        async getCustomers({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-customers?page=${payload.page}`, payload)
                commit('setCustomers', data)
            } catch (e) {
                console.log(e)
            }
        },

        async getCustomerList({commit}) {
            try {
                const {data} = await axios.get('/get-customer-list')
                commit('setCustomerList', data)
            } catch (e) {
                console.log(e)
            }
        },

        async getCustomer({commit}, id) {
            try {
                if(id){
                    const {data} = await axios.get(`/get-customer/${id}`)
                    commit('setCustomer', data)
                }
            } catch (e) {
                console.log(e)
            }
        },

        async getOtherLocations({commit},payload = {id: null, page: 1, search: null}) {
            try {
                if(payload.id){
                    const {data} = await axios.post(`/get-other-locations`, payload)
                    commit('setOtherLocations', data)
                }
            } catch (e) {
                console.log(e)
            }
        },

        async getPeoples({commit},payload = {id: null, page: 1, search: null}) {
            try {
                if(payload.id){
                    const {data} = await axios.post(`/get-peoples`, payload)
                    commit('setPeoples', data)
                }
            } catch (e) {
                console.log(e)
            }
        },
    },
}
