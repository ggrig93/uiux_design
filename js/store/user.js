export default {
    namespaced: true,

    state () {
        return {
            ownerUsers: [],
        }
    },

    getters: {
        ownerUsers: state => state.ownerUsers,
    },

    mutations: {
        setOwnerUsers(state, value) {
            state.ownerUsers = value
        },
    },

    actions: {
        async getOwnerUsers({commit}) {
            try {
                const {data} = await axios.get('/get-owner-users')
                commit('setOwnerUsers', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
    },
}
