export default {
    namespaced: true,

    state () {
        return {
            countryList: {},
        }
    },

    getters: {
        countryList: state => state.countryList,
    },

    mutations: {
        setCountryList(state, value) {
            state.countryList = value
        },
    },

    actions: {
        async getCountryList({commit, state}) {
            try {
                if(Object.keys(state.countryList).length === 0){
                    const {data} = await axios.get('/get-country-list')
                    commit('setCountryList', data)
                }
            } catch (e) {
                console.log(e)
            }
        },
    },
}
