export default {
    namespaced: true,

    state () {
        return {
            invoices: [],
            invoiceStatuses: [],
            invoiceNotifications: []
        }
    },

    getters: {
        invoices: state => state.invoices,
        invoiceStatuses: state => state.invoiceStatuses,
        invoiceNotifications: state => state.invoiceNotifications

    },

    mutations: {
        setInvoices(state, value) {
            state.invoices = value
        },
        setInvoiceStatuses(state, value) {
            state.invoiceStatuses = value
        },
        setInvoiceNotifications(state, value) {
            state.invoiceNotifications = value
        },
    },

    actions: {
        async getInvoices({commit}, payload = {page: 1, search: null}) {
            try {
                const {data} = await axios.post(`/get-invoices?page=${payload.page}`, payload)
                commit('setInvoices', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
        async getInvoiceStatuses({commit}) {
            try {
                const {data} = await axios.get('/get-invoice-statuses')
                commit('setInvoiceStatuses', data)
            } catch (e) {
                console.log(e)
            }
        },
        async getInvoiceNotifications({commit}, limitEmail) {
            try {
                const {data} = await axios.post('/get-invoice-notifications', {limit: limitEmail})
                commit('setInvoiceNotifications', data)
            } catch (e) {
                console.log(e)
            }
        },
    },
}
