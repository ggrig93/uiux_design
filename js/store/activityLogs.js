export default {
    namespaced: true,

    state () {
        return {
            activityLogs: [],
        }
    },

    getters: {
        activityLogs: state => state.activityLogs,
    },

    mutations: {
        setActivityLogs(state, value) {
            state.activityLogs = value
        },
    },

    actions: {
        async getActivityLogs({commit}, limit) {
            try {
                const {data} = await axios.post('/get-activity-logs', {limit: limit})
                commit('setActivityLogs', data)
                return data
            } catch (e) {
                console.log(e)
            }
        },
    },
}
